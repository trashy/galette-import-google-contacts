<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Google contact client class. Encapsulates the google client
 * functionality specific for contacts.
 *
 * This page can be loaded directly, or via ajax.
 * Via ajax, we do not have a full html page, but only
 * that will be displayed using javascript on another page
 *
 * PHP version 5
 *
 * Copyright © 2011-2013 The Galette Team
 *
 * This file is part of Galette (http://galette.tuxfamily.org).
 *
 * Galette is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Galette is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
* along with Galette. If not, see <http://www.gnu.org/licenses/>.
*
* @category  Plugins
* @package   GaletteImportGmailContacts
* @author    Marc Andreu Fernandez <marcandreuf@gmail.com>
* @copyright 2011-2013 The Galette Team
* @license   http://www.gnu.org/licenses/gpl-3.0.html GPL License 3.0 or (at your option) any later version
* @version   SVN: $Id: owners.php 556 2009-03-13 06:48:49Z trashy $
* @link      http://galette.tuxfamily.org
* @since     Available since 0.7dev - 2011-06-02
*/

require_once '_config.inc.php';

/** Load google client api library.
 * Documentation: http://code.google.com/apis/gdata/docs/2.0/basics.html
 */
require_once 'lib/google-api-php-client/src/Google_Client.php';

/**
 * Member class for galette
 *
 * @category  Utility
 * @name      GoogleContactsClient
 * @package   GaletteImportGmailContacts
 * @author    Marc Andreu Fernandez <marcandreuf@gmail.com>
 * @copyright 2009-2013 The Galette Team
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GPL License 3.0 or (at your option) any later version
 * @link      http://galette.tuxfamily.org
 * @since     Available since 0.7dev - 02-06-2009
 */
class GoogleContactsClient
{
    private $_client;

    private $_client_config = array(
        'applicationName'   => 'your app name here',
        'scopes'            => 'http://www.google.com/m8/feeds/',
        'clientId'          => '???????',
        'clientSecret'      => '??????',
        'redirectUri'       => '???????',
        'developerKey'      => '???????'
    );

    /**
     * Constructor
     *
     * @param string $host host name to redirect the google request.
     */
    function __construct($host)
    {
        $this->_client = new Google_Client();
        $this->_client->setApplicationName($this->_client_config['applicationName']);
        $this->_client->setScopes($this->_client_config['scopes']);
        $this->_client->setClientId($this->_client_config['clientId']);
        $this->_client->setClientSecret($this->_client_config['clientSecret']);
        //FIXME: protocol must not be hardcoded
        $hostFullName = 'http://'.$host.$this->_client_config['redirectUri'];
        $this->_client->setRedirectUri($hostFullName);
        $this->_client->setDeveloperKey($this->_client_config['developerKey']);
    }

    /**
     * Exchange request code for the oauth token.
     *
     * @param session &$session current user session.
     *
     * @return void
     */
    public function exchangeCodeForToken(&$session)
    {
        $this->_client->authenticate();
        $session['token'] = $this->_client->getAccessToken();
    }

    /**
     * Update client object with session token.
     *
     * @param ref $session user session reference
     *
     * @return void
     */
    public function updateClientTokenFromSession($session)
    {
        if (isset($session['token'])) {
            $this->_client->setAccessToken($session['token']);
        }
    }

    /**
     * Logout client.
     *
     * @param session &$session current user session.
     *
     * @return void
     */
    public function logoutUserSession(&$session)
    {
        unset($session['token']);
        $this->_client->revokeToken();
    }

    /**
     * Check if client is connected to google oauth.
     *
     * @return boolean
     */
    public function isConnected()
    {
        if ($this->_client->getAccessToken()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the href url with all paramters to connect to google oauth.
     *
     * @return string
     */
    public function getConnectUrl()
    {
        return $this->_client->createAuthUrl();
    }

    /**
     * Get contact list of a given group.
     *
     * @param string $user       gmail user email
     * @param string $groupId    contacts group id
     * @param string $maxResults max number of results
     *
     * @return list of contacts
     */
    public function getContacts($user, $groupId, $maxResults)
    {
        $contactsGroup = "http://www.google.com/m8/feeds/groups/".
            $user."/base/".$groupId;

        $reqUrl = "https://www.google.com/m8/feeds/contacts/default/full".
            "?max-results=".$maxResults.
            "&group=".$contactsGroup;

        $req = new Google_HttpRequest($reqUrl);
        $val = $this->_client->getIo()->authenticatedRequest($req);

        $xml = simplexml_load_string($val->getResponseBody());
        if ($xml === false) {
            throw new Exception("Empty contacts data.");
        }
        $xml->registerXPathNamespace('a', 'http://www.w3.org/2005/Atom');
        $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
        $entries = $xml->xpath('//a:entry');

        return $entries;
    }
}
