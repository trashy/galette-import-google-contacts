{if $login->isAdmin()}
{* Block title *}
<h1 class="nojs">{_T string="Google contacts"}</h1>
{* Menu entries *}
<ul>
   <li{if $PAGENAME eq "fetchGoogleContacts.php"} class="selected"{/if}><a href="{$galette_base_path}{$galette_galette_import_google_contacts_path}fetchGoogleContacts.php">{_T string="Import"}</a></li>
</ul>
{/if}
