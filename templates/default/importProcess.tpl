{$errorMsg}
</br>
{_T string = "Request for {$maxResults} results."}
</br>
{_T string = "Results to import: {$numOfEntries}."}
</br>
{_T string = "PROCESSED CONTACTS LIST:"}</br>
<ul>
  {foreach $processedList as $name}
    <li>{$name}</li>
  {/foreach}
</ul>
</br>
{_T string = "NOT PROCESSED CONTACTS LIST:"}</br>
<ul>
  {foreach $notProcessedList as $notProcName}
    <li>{$notProcName}</li>
  {/foreach}
</ul>
</br>
{_T string = "Processed contacts: {$countProcessed}."}</br>
{_T string = "Not processed contacts: {$countNotProcessed}."}</br>
</br>
<a class=logout href="{$logoutUrl}">{_T string="Logout from google."}</a>
