<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Add member test
 *
 * This page can be loaded directly, or via ajax.
 * Via ajax, we do not have a full html page, but only
 * that will be displayed using javascript on another page
 *
 * PHP version 5
 *
 * Copyright © 2011-2013 The Galette Team
 *
 * This file is part of Galette (http://galette.tuxfamily.org).
 *
 * Galette is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Galette is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Galette. If not, see <http://www.gnu.org/licenses/>.
*
* @category  Plugins
* @package   GaletteImportGmailContacts
* @author    Marc Andreu Fernandez <marcandreuf@gmail.com>
* @copyright 2011-2013 The Galette Team
* @license   http://www.gnu.org/licenses/gpl-3.0.html GPL License 3.0 or (at your option) any later version
* @version   SVN: $Id: owners.php 556 2009-03-13 06:48:49Z trashy $
* @link      http://galette.tuxfamily.org
* @since     Available since 0.7dev - 2011-06-02
*/

define('GOOGLECONTACT_PREFIX', 'googlecontact_');
define('GOOGLECONTACT_SMARTY_PREFIX', 'plugins_googlecontact');

/** Load google client api library.
* Documentation: http://code.google.com/apis/gdata/docs/2.0/basics.html
* Visit https://code.google.com/apis/console?api=contacts to generate your
* oauth2_client_id, oauth2_client_secret, and register your oauth2_redirect_uri.
*/


?>