<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Add member test
 *
 * This page can be loaded directly, or via ajax.
 * Via ajax, we do not have a full html page, but only
 * that will be displayed using javascript on another page
 *
 * PHP version 5
 *
 * Copyright © 2011-2013 The Galette Team
 *
 * This file is part of Galette (http://galette.tuxfamily.org).
 *
 * Galette is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Galette is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Galette. If not, see <http://www.gnu.org/licenses/>.
*
* @category  Plugins
* @package   GaletteImportGmailContacts
* @author    Marc Andreu Fernandez <marcandreuf@gmail.com>
* @copyright 2011-2013 The Galette Team
* @license   http://www.gnu.org/licenses/gpl-3.0.html GPL License 3.0 or (at your option) any later version
* @version   SVN: $Id: owners.php 556 2009-03-13 06:48:49Z trashy $
* @link      http://galette.tuxfamily.org
* @since     Available since 0.7dev - 2011-06-02
*/



// set the required constant
define('GALETTE_BASE_PATH',  '../../');
// include the main file slab
require_once GALETTE_BASE_PATH . 'includes/galette.inc.php' ;

if (!$login->isLogged() || !$login->isAdmin()) {
    header('location: ' . GALETTE_BASE_PATH . 'index.php');
    die();
}


/**
 * Sample data
 */
$email = "testuser@gmail.com";
//TODO: define all sample data variables, I will do later

/**
 * Create user object.
 */
$adherent = new Galette\Entity\Adherent();


//Fetching user by email or username if it exists.
$adherent->loadFromLoginOrMail($email);


$import_values = array(
        //'pseudo_adh' => $line[1], 
        'login_adh' => "testuser", // I guess this is user name 
        'nom_adh' => utf8_encode(trim("simpleuser")),
        'prenom_adh' => utf8_encode(trim("ifmaywork")),
        //'adresse_adh' => $addr1,
        //'adresse2_adh' => $addr2,
        //'cp_adh' => $line[5],
        //'ville_adh' => utf8_encode(trim($line[6])),
        //'pays_adh' => 'France',
        'email_adh' => trim($email),
        //'ddn_adh' => dateAccessToString($line[8]),
        //'titre_adh' => $line[9] ? 1 : 2, 
        //'tel_adh' => trim($line[10]),
        //'gsm_adh' => trim($line[11]),
        //'date_echeance' => dateAccessToString($line[18]),
        //'activite_adh' => false, 
        //'id_statut' => 4, 
        //'bool_display_info' => 0,
        //'pref_lang' => 'fr_FR', // Change to english for now
        //'info_adh' => str_replace('@#@', '\n', utf8_encode(trim($line[25]))),
        //'info_public_adh' => str_replace('@#@', '\n', utf8_encode(trim($line[25]))),
);

$import_values['id_statut'] = Galette\Entity\Status::DEFAULT_STATUS;

//Check values before storeing.
$ok = $adherent->check($import_values, array(), array());

//Store user if its all right.
if ($ok) {
    $adherent->store();
}
