* Google Accounts Authentication and Authorization OAuth 2.0, see all documentation at
  https://developers.google.com/accounts/docs/OAuth2Login

* To get the contact details this plugin uses the Google Contacts API version 3.0
  https://developers.google.com/google-apps/contacts/v3/
  Specifically using the request to get all contacts from a given group
  https://developers.google.com/google-apps/contacts/v3/#retrieving_all_contact_groups
  https://developers.google.com/google-apps/contacts/v3/reference#Parameters

* To the groupId, go to the gmail account, go to Contacts and select the group, 
  then check the url and the group id is the numeric code after "/group" for example:
  https://mail.google.com/mail/u/0/?shva=1#contacts/group/<groupId>/<groupName>
  Or use the Google contacts API to retrive the groups of the account.

  In order to you the plugin you only need to setup the values at GoogleContactsClient class. 

     private $_client_config = array(
             'applicationName' => 'your app name here',
             'scopes' => 'http://www.google.com/m8/feeds/',
             'clientId' => '???????', //Client ID at the console
             'clientSecret' => '??????',  //Client secret at the console  
             'redirectUri' => '???????', // Redirect URIs at the console
             'developerKey' => '???????' //API key at the console at the Simple API Access
     );

  Add the values missing. You can get all those values at the google console, 
  https://code.google.com/apis/console
  Go to "API Access", create a new client ID and add the redirection URIs to the 
  http://<yourHost>/galette/galette/plugins/galette-import-google-contacts/fetchGoogleContacts.php
  Create as well a Simple API Access if you do not have already one.

  
