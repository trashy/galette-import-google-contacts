<?php

// This is just the simple test to play with atoum.

namespace galette\googleContacts\tests\units;

require_once '../mageekguy.atoum.phar';
include '../../classes/helloWorld.php';

$_SERVER['HTTP_HOST'] = "test";
require_once '../../_config.inc.php' ;

use \mageekguy\atoum;
use galette\googleContacts;

class helloWorld extends atoum\test
{
    public function testSay()
    {
        $helloWorld = new googleContacts\helloWorld();

        $this->assert->string($helloWorld->say())->isEqualTo('Hello World!');
    }
}

?>