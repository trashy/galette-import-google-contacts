<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Main script for fetching google contacts.
 *
 * This page can be loaded directly, or via ajax.
 * Via ajax, we do not have a full html page, but only
 * that will be displayed using javascript on another page
 *
 * PHP version 5
 *
 * Copyright © 2011-2013 The Galette Team
 *
 * This file is part of Galette (http://galette.tuxfamily.org).
 *
 * Galette is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Galette is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Galette. If not, see <http://www.gnu.org/licenses/>.
*
* @category  Plugins
* @package   GaletteImportGmailContacts
* @author    Marc Andreu Fernandez <marcandreuf@gmail.com>
* @copyright 2011-2013 The Galette Team
* @license   http://www.gnu.org/licenses/gpl-3.0.html GPL License 3.0 or (at your option) any later version
* @version   SVN: $Id: owners.php 556 2009-03-13 06:48:49Z trashy $
* @link      http://galette.tuxfamily.org
* @since     Available since 0.7dev - 2011-06-02
*/

// Necessary to show chars on page.
header('Content-Type: text/html; charset=utf8');

// set the required constant for galette
define('GALETTE_BASE_PATH',  '../../');
// include the main file slab
require_once GALETTE_BASE_PATH . 'includes/galette.inc.php';
require_once 'GoogleContactsClient.php';

/**
 * Check if the there is a code parameter perform the authentication
 * process of oauth. And redirect to same page with the new
 * access token.
 *
 * @param client $client The google client php api object.
 *
 * @return void
 */
function checkGoogleRedirectionCode($client)
{
    if (isset($_GET['code']) ) {
        $client->exchangeCodeForToken($_SESSION);
        $redirect = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        header('Location: '.filter_var($redirect, FILTER_SANITIZE_URL));
    }
}

/**
 * Perform logout if requested by parameter.
 *
 * @param client $client The google client php api object.
 *
 * @return void
 */
function checkLogoutParameter($client)
{
    if (isset($_REQUEST['logout'])) {
        $client->logoutUserSession($_SESSION);
        $redirect = 'http://'.$_SERVER['HTTP_HOST'].
        "/galette/galette/gestion_adherents.php";
        header('Location: '.filter_var($redirect, FILTER_SANITIZE_URL));
    }
}

/** Check the user is logged in and has admin rights. */
if (!$login->isLogged()) {
    header('location: index.php');
    die();
} elseif (!$login->isAdmin() && !$login->isStaff() && !$login->isGroupManager()) {
    header('location: voir_adherent.php');
    die();
}

$googleContactsClient = new GoogleContactsClient($_SERVER['HTTP_HOST']);
checkGoogleRedirectionCode($googleContactsClient);
$googleContactsClient->updateClientTokenFromSession($_SESSION);
checkLogoutParameter($googleContactsClient);

if ($googleContactsClient->isConnected()) {
    //FIXME: plugin URL must not be hardcoded
    //FIXME: protocol must not be hardcoded
    $redirect = "http://"
        .$_SERVER['HTTP_HOST'].
        "/galette/galette/plugins/galette-import-google-contacts/".
        "importForm.php";
    header('Location: '.filter_var($redirect, FILTER_SANITIZE_URL));
} else {
    $tpl->assign('require_dialog', true);
    $tpl->assign('page_title', _t("import google contacts"));
    $orig_template_path = $tpl->template_dir;
    $tpl->template_dir = 'templates/'.$preferences->pref_theme;

    $tpl->assign('authUrl', $googleContactsClient->getConnectUrl());
    $content = $tpl->fetch('fetchGoogleContacts.tpl', GOOGLECONTACT_SMARTY_PREFIX);
    $tpl->assign('content', $content);

    $tpl->template_dir = $orig_template_path;
    $tpl->display('page.tpl', GOOGLECONTACT_SMARTY_PREFIX);
}
